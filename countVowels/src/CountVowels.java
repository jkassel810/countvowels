/**
 * Created by jkass on 11/26/2016.
 *
 * Count Vowels – Enter a string and the program counts the number of vowels in the text.
 * For added complexity have it report a sum of each vowel found.
 */
public class CountVowels {


    public static int[] countVowels (String myString){

        String[] vowelList = {"a", "e", "i", "o", "u"};

        char[] s2char = myString.toCharArray();
        int aStr = 0;
        int eStr = 0;
        int iStr = 0;
        int oStr = 0;
        int uStr = 0;
        for(int i=0; i<s2char.length; i++){

            for(int j=0; j<vowelList.length; j++){

                if (String.valueOf(s2char[i]).equals(vowelList[j])) {
                    switch (s2char[i]) {
                        case 'a':
                        aStr++;
                        break;
                        case 'e':
                        eStr++;
                        break;
                        case 'i':
                        iStr++;
                        break;
                        case 'o':
                        oStr++;
                        break;
                        case 'u':
                        uStr++;
                        break;
                    }
                }

            }
        }
        int[] retArray = {aStr, eStr, iStr, oStr, uStr};
        return retArray ;
    }



    public static void main (String[] args){

        String testString = "beautiful";

        System.out.println("Testing String: " + testString);

        int[] numVowels = countVowels(testString);

        System.out.println("Number of 'a\'s': " + numVowels[0]);
        System.out.println("Number of 'e\'s': " + numVowels[1]);
        System.out.println("Number of 'i\'s': " + numVowels[2]);
        System.out.println("Number of 'o\'s': " + numVowels[3]);
        System.out.println("Number of 'u\'s': " + numVowels[4]);
        int totVowels = numVowels[0]+numVowels[1]+numVowels[2]+numVowels[3]+numVowels[4];
        System.out.println("Total number of Vowels Found: "+ totVowels);

    }


}
